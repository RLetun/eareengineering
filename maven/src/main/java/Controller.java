import model.Model;
import model.eaModelHash.EAElement;
import model.eaModelHash.EAView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by romanletun on 15.11.2016.
 * Программа предназначена для автоматизации процесса реинжиниринга базы данных ЦМИОР.
 * Реинжинирятся таблицы, вью, хранимые процедуры и их поля и параметры + ключи таблиц.
 * Предпалагается что комментарии к таблицам и их полям хрянятся в extendedProperty,
 * а комментарии к вью и процедурам в коде вью и процедур. Формат таких комментариев описан в класса Parser.
 */

public class Controller
{
    private static String connectionString = "EnArchitect --- DBType=1;ConnectEx=X\\Xbid\"b'5xn?edAHMrcMN[M8l0:GPp+vKnYX.w]@Vk)1q+>*(./X1Ggxfd4dbT*N87c?<&|Z1q'JAaMn*qEt$$D|\\<F%)'6mhQsFs&.$#ly>\\unw>{)A!QVLXp7L\\?Qmzww0kDyqOxM";
    //private static String connectionString = "C:\\Cascade\\branches\\romanletun\\Cascade.eap"; //Конект к модели.
    private static String schema = "";
    private static String packageId = "";
    private static boolean updateNullComments = true;
    private static Map<String,String> params = new HashMap<>();
    //Логгер
    private static Logger logger = LoggerFactory.getLogger(Controller.class);

    public static void main(String[] args)
    {
        logger.info("Работаю..");
        //Засекаю время выполнения программы
        Date date = new Date();

        //Входные параметры
        initParams(args);

        try (Model model = new Model(connectionString))
        {
            model.setUpdateNullComments(updateNullComments);
            logger.info("EA загрузился!");
            model.reengineering(Integer.parseInt(packageId), schema);
            //model.reengineering(1716, "");
            model.close();
            System.out.println();
            logger.info("Готово!");
            logger.info("Реинжениринг выполнен за " + new Date(new Date().getTime() - date.getTime()).getMinutes() + " минут(ы).");
        } catch (NumberFormatException nfe)
        {
            logger.error("Неверный ID пакета Enterprise Architect.", nfe);
        }
        catch (Exception e)
        {
            logger.error("Unknown error", e);
        }
    }

    public static void printHelp()
    {
        System.out.println("Программа предназначена для автоматизации процесса реинжиниринга базы данных ЦМИОР (CMIOR_A).\n" +
                " Реинжинирятся таблицы, вью, хранимые процедуры и их поля и параметры + ключи таблиц.\n" +
                " Предпалагается что комментарии к таблицам и их полям хрянятся в extendedProperty,\n" +
                " а комментарии к вью и процедурам в коде вью и процедур." +
                "По умолчанию реинжениринг производится в модель EA на сервере ZSQL." +
                "Программа запускается с набором именнованых параметров:" +
                " Обязательные входные праметры: " +
                "  -p:\"Идентификатор пакета EA\". Например 1192" +
                " Необязательные параметры: " +
                "  -s:\"Наименование схемы данных БД\"." +
                "  -c:\"connectionString для подключения к модели EA\"." +
                "  -e Меняет логику реинжениринга комментариев к объектам, если параметр передан, то в ситуации когда объект в модели имеет комментарий, " +
                "    а в БД комментария нет, комментарий из модели НЕ будет удалён. По умолчанию такие комментарии будут удаляться!" +
                "Если программа запущена только с одним параметром (ИД пакета), то предполагается что он содержит в себе пакеты," +
                " чьи имена соответствуют схемам данных в БД. В таком случае будет произведён реинжениринг всей БД." +
                "Если программа запущена с двумя параметрами (ИД пакета и наименование схемы), " +
                " то в пакет будет проинженирена указанная схема данных");
    }

    public static void initParams(String[] args) {
        for (String s : args) {
            if (s.split(":").length > 1)
                params.put(s.split(":")[0], s.split(":")[1]);
            else
                params.put(s,"");
        }

        if (params.containsKey("-p"))
            packageId = params.get("-p");

        if (params.containsKey("-s"))
            schema = params.get("-s");

        if(params.containsKey("-c"))
            connectionString = params.get("-c");

        if (params.containsKey("-e"))
            updateNullComments = false;
    }
}
