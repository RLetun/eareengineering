package exceptions;

/**
 * Created by romanletun on 16.11.2016.
 * Кидаю когда по каким-то причинам не удаётся подключиться к модели EA.
 */
public class ConnectionException extends Exception {
    public ConnectionException(String message) {
        super(message);
    }
}
