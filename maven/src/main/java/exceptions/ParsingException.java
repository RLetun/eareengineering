package exceptions;

/**
 * Created by romanletun on 15.11.2016.
 * Ошибки при парсинге кода ХП и view на предмет комментариев.
 */
public class ParsingException extends Exception {
    public ParsingException(String message) {
        super(message);
    }
}
