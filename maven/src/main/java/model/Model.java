package model;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.eaModelHash.*;
import model.DBElements.*;
import model.DBElements.Key;
import exceptions.ConnectionException;
import exceptions.ParsingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sparx.*;
import org.sparx.Collection;
import org.sparx.Package;

import java.io.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by romanletun on 15.11.2016.
 * Основная логика программы.
 * Возможны 2 сценария реинжениринга:
 *  1 - Реинженирится вся база целиком.
 *  2 - Реинженирится одна схема данных.
 * По какому сценарию производить реинжениринг опредяют входные параметры public метода reengineering().
 *  Подробнее в описании метода.
 * Если реинженирится вся база, то:
 * 1.Для каждого пакета в "rootPackage", ищю в базе схему данных с соотвествующим наименованием.
 * 2.Выбираю все Таблицы, вью и хп.
 * 3.Для найденых таблиц, вью и хп либо создаю их в модели, либо обновляю, если они уже существуют.
 * 4.Далее выбираю поля таблиц вью и таблиц и параметры процедур. Для них так же либо добавляю либо обновляю.
 * 5.Выбираю ключи таблиц.
 * 6.Из кода вью и процедур выбираю комментарии.
 * 7.Profit!
 *
 * Для реинжениринга одной схемы алгоритм идентичный, меняется только первый пункт:
 * 1.В пакет "rootPackage" реинженирится схема "schema"
 *
 * В этом классе собраны все опрерации с моделью EA.
 */
public class Model implements Closeable
{
    //private final int rootPackageID = 1192;
    //private final int rootPackageID = 1716; //ИД корневого пакета. Пока константа, но должен быть входным параметром в main
    private DAO dao;
    private Repository repository; //Основной класс для работы с EA
    private boolean updateNullComments = false; //Если true, то в ситуации когда в модели у объекта есть комментарий, а в БД нету, то комментарий в модели перезатрётся.
    private static int connectorsCounter = 0; //Счётчиек коннекторов, пусть пока будет здесь.
    private static List<DBConnector> connectors = new ArrayList<>(); // Коннекторы
    //Логгер
    private static Logger logger = LoggerFactory.getLogger(Model.class);

    public Model(String connectionString) {
        connect(connectionString);
    }

    //Подключение к базе данных и к enterprise architect
    private void connect(String connectionString)
    {
        try
        {
            dao = new DAO();

            repository = new Repository();
            if (!repository.OpenFile(connectionString))
                throw new ConnectionException("Ошибка при открытии файла модели или при попытке подключиться к серверу модели.");
        }
        catch (Exception e)
        {
            logger.error("Error", e);
            repository.Exit();
            dao.close();
            System.exit(2);
        }
    }

    public void close()
    {
        repository.CloseFile();
        repository.Exit();
        dao.close();
    }

    //Основной метод для реинжиниринга
    //rootPackageID - ID пакета в который произдится реинжениринг БД
    //schema - Наименование схемы БД для реинжениринга. Если параметр пустой реинженирится вся база!
    public void reengineering(int rootPackageID, String schema)
    {
        Package rootPackage; //Корневой пакет в который проиводится реинжениринг. Должен содержать в себе пакеты с именами соотвествующими именам схем данных в БД

        try {
            rootPackage = repository.GetPackageByID(rootPackageID); //Получаем ссылку на пакет в enteprise architect в который производится реинжинирирнг.
            List<Schema> schemas = getDBSchemas(); //Список схем из БД.
            List<EAPackage> packages = new ArrayList<>(); //Фильтр по пакетам для реинжениринга.

            //Если вторым параметром передана реальная схема данных, то реинженирить буду только её.
            //Для этого в списке схем оставляю только одну нужную схему, а в packages кладу пакет EA,
            // в который будет производится реинжениринг.
            if (schema != null && !"".equals(schema) && schemas.contains(new Schema(schema)))
            {
                schemas.clear();
                schemas.add(new Schema(schema));

                packages.add(new EAPackage(rootPackage.GetName(), getPackageHash(rootPackage), rootPackageID));
            }

            //Реинжиниринг БД.
            dao.reengineering(schemas);
            DBModel dbModel = new DBModel(schemas, dao.getUDT());
            logger.info("БД прореинженирина");

            //Модель EA
            EAModel eaModel;
            //EAModel eaModel = deserializeEAModel(rootPackageID); // Проверяю, есть ли сохранённый .json файл с сериализованной моделью
            //if (eaModel == null) // Если нет, то получаю модель из EA.

            //Если в список packages был добавлен хотябы один пакет, то реинженирю только в них
            if (packages.size() > 0) {
                eaModel = new EAModel();
                eaModel.setPackages(packages);
                //eaModel.setUdt(getUDT(rootPackage));
            }
            else { //Если нет то готовлюсь к реинженирингу всей базы.
                eaModel = getModelHash(rootPackage);
                eaModel.setUdt(getUDT(rootPackage));
            }

            /*
              На данном этапе в программу загружены данные из БД по тем схемам которые надо реинженирить (schemas)
               и модель EA для сравнения с БД.
             */

            logger.info("Получен хеш модели");

            //Сначала реинженирю инфорацию о UDT
            try {
                if (dbModel.getUdt().hashCode() != eaModel.getUdt().hashCode()) {
                    label0:
                    for (Column c : dbModel.getUdt().getColumns()) {
                        for (EAAttribute a : eaModel.getUdt().getAttrs()) {
                            if (c.getName().equals(a.getName())) {
                                if (c.getHash() != a.getHash()) {
                                    logger.info("Обновляю пользовательский тип данных: \"" + a.getName() + "\"");
                                    updateAttribute(c, a.getId(), a);
                                }

                                continue label0;
                            }
                        }
                        logger.info("Добавляю пользовательский тип данных: \"" + c.getName() + "\"");
                        addAttribute(c, repository.GetElementByID(eaModel.getUdt().getId()).GetAttributes(), eaModel.getUdt().getAttrs());
                    }
                }

                addUDTinModel(dbModel.getUdt());

            } catch (Exception e) {
                logger.error("Не удалось прореинженирить UDT.", e);
            }

            //В следущей куче вложенных циклов сравниваю имена объектов и хешкоды.
            //Если имена объектов совпадают, а хешкоды нет, то объекты отличаются и необходимо выполнить ренжиниринг.
            //Если в базе есть объекты, для которых нет соответствия в моделе EA, то добавляем их в EA.
            //Если наоборот, то удаляю из EA.
            //Выглядит страшно.
            for (Schema s : dbModel.getSchemas()) {
                for (EAPackage p : eaModel.getPackages()) {
                    if (s.getName().equals(p.getName())) {//&& s.hashCode() != p.getHash()
                        label0:
                        for (DBObject o : s.getObjects()) {
                            for (EAElement e : p.getElements()) {
                                if (o.getName().equals(e.getName())) {
                                    e.setIsDeleted(false);// Для этого елемента есть аналог в БД, его удалять не надо.

                                    //if (o.hashCode() != e.hashCode()) {
                                        logger.info("Обновляю элемент: " + o.getSchema() + "." + o.getName());
                                        updateElement(o, e.getId(), e);

                                        if (o.getStereoType().equals("view") || o.getStereoType().equals("procedure"))//Для вью и процедур поля не ринженирю
                                            continue label0;

                                        label1:
                                        for (Column c : o.getColumns()) {
                                            for (EAAttribute a : e.getAttrs()) {
                                                if (c.getName().equals(a.getName())) {
                                                    a.setIsDeleted(false);// Для этого поля есть аналог в БД, его удалять не надо.

                                                    if (c.getHash() != a.getHash()) {
                                                       logger.info("Обновляю атрибут: \"" + o.getStereoType() + "\" " + p.getName() + "." + e.getName() + "." + a.getName());
                                                       updateAttribute(c, a.getId(), a);
                                                    }

                                                    continue label1;
                                                }
                                            }
                                            //Выполниться только если колонка "c" отсутствует в моделе EA
                                            logger.info("Добавляю аттрибут: " + s.getName() + "." + o.getName() + "." + c.getName());
                                            addAttribute(c, repository.GetElementByID(e.getId()).GetAttributes(), e.getAttrs());
                                        }
                                    //}

                                    continue label0;
                                }
                            }
                            //Выполниться только если объект "o" отсутствует в моделе EA
                            logger.info("Добавляю элемент: " + s.getName() + "." + o.getName());
                            addElement(o, repository.GetPackageByID(p.getId()).GetElements(), p.getElements());
                        }
                    }
                }
            }

            //Ищу и удаляю объекты, которые есть в моделе EA, но отсутствуют в БД
            findDeletedObjects(eaModel);

            addConnectors(eaModel);

            //Сериализую модель, что бы в следующий раз не лазить за ней в EA.
            //serializeEAModel(eaModel);
        }
        catch (Exception e)
        {
            this.close();
            logger.error("Error", e);
        }
    }

    //Десириализация модели из json файла.
    //Файл должен иметь имя "EAModel.json" и располагаться в том же каталоге что и файл класса "Model".
    /*
    private EAModel deserializeEAModel(int rootPackageID)
    {
        EAModel model = null;

        try
        {
            //Если работаю НЕ с локальным файлом модели, то можно десериализовать модель из файла и проверить по датам, что десириализованная версия актуальна.
            if (!connectionString.contains("\\")) {
                FileReader fileReader = new FileReader(new File(Model.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent() + "\\EAModel.json");
                //FileReader fileReader = new FileReader("C:\\Users\\romanletun@fintech.ru\\IdeaProjects\\EAReengineering\\out\\production\\EAReengineering\\EAModel.json");

                ObjectMapper mapper = new ObjectMapper();

                model = mapper.readValue(fileReader, EAModel.class);


                Date lastModelModifiedDate = dao.getLastModifiedDate(rootPackageID);

                //Если дата последнего изменения модели больше чем дата последней сериализации, то значит кто-то поправил модель руками.
                //Следовательно необходимо заново считывать hash модели из Enterprise Architect.
                if (model.getHashDate().getTime() < lastModelModifiedDate.getTime())
                    return null;
            }

        } catch (Exception e)
        {
            //System.out.println("Ошибка при десериализации модели EA.");
            //e.printStackTrace();
            return null;
        }

        return model;
    }
    */

    //Возвращает список схем данных из БД.
    private List<Schema> getDBSchemas()
    {
        return dao.getSchemas();
    }

    //Сериализую модель в json формате, в каталог где лежит .jar файл (От класса model на 3 каталога в верх), под именем "EAModel.json".
    private void serializeEAModel(EAModel model)
    {
        try {
            Path path = Paths.get(new File(Model.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent() + "\\EAModel.json");

            if (Files.exists(path))
                Files.delete(path);

            Path file = Files.createFile(path);

            FileWriter fileWriter = new FileWriter(file.toFile());

            model.setHashDate(new Date()); //Устанавливаем текущую дату в качестве даты сериализации.

            ObjectMapper mapper = new ObjectMapper();

            mapper.writeValue(fileWriter, model);

            fileWriter.close();

        } catch (Exception e)
        {
            logger.error("Ошибка при сериализации модели EA.", e);
        }
    }

    //Проверяю что таблица, вью или ХП из модели присутсвует в БД.
    private void findDeletedObjects(EAModel model) {

        ArrayList<EAElement> elements;
        ArrayList<EAAttribute> attributes;

        for (EAPackage p : model.getPackages()) {
            elements  = new ArrayList<>(p.getElements());//Копия списка

            for (EAElement e : elements) {
                if (e.isDeleted()) {
                    logger.info("Удаляю элемент: " + e.getName());
                    deleteElement(e);
                    p.getElements().remove(e);
                }
                else {
                    attributes = new ArrayList<>(e.getAttrs());//Копия списка

                    for (EAAttribute a : attributes) {
                        if (a.isDeleted()) {
                            logger.info("Удаляю атрибут: " + a.getName());
                            deleteAttribute(a);
                            e.getAttrs().remove(a);
                        }
                    }
                }
            }
        }
    }

    private void deleteElement(EAElement element)
    {
        Package pack = repository.GetPackageByID(repository.GetElementByID(element.getId()).GetPackageID());

        for (short i = 0; i < pack.GetElements().GetCount(); i++)
        {
            Element e = pack.GetElements().GetAt(i);
            if (e.GetName().equals(element.getName()))
                pack.GetElements().Delete(i);
        }

        pack.GetElements().Refresh();
        pack.Update();
    }

    private void deleteAttribute(EAAttribute attribute)
    {
        Element element = repository.GetElementByID(repository.GetAttributeByID(attribute.getId()).GetParentID());

        for (short i = 0; i < element.GetAttributes().GetCount(); i++) {
            Attribute att = element.GetAttributes().GetAt(i);
            if (att.GetName().equals(attribute.getName()))
                element.GetAttributes().Delete(i);
        }

        element.GetAttributes().Refresh();
        element.Update();
    }

    //Метод добавляет таблицу, вью или процедуру (element), полученную из БД, в колекцию  DBElements, в которй они хранятся в EA.
    //После этого добавляет или обновляет данные о полях element'а =>  addElement(column, newElement.GetAttributes());
    //И выбирает комментарии для вью и хп. => Parser.getElementComment(newElement, element.getDefinition());
    private void addElement(DBObject dbElement, Collection<Element> elements, List<EAElement> eaElements)
    {
        Element newElement = elements.AddNew(dbElement.getName(), "Class");

        newElement.SetStereotype(dbElement.getStereoType());
        newElement.SetNotes(dbElement.getNotes());
        newElement.SetGentype("SQL Server 2008");
        newElement.Update();

        //Для таблиц реинженирю данные о полях входящих в первичный, альтернативный и внешний ключи.
        if (dbElement.getStereoType().equals("table")) {
            Table table = (Table)dbElement;

            for (Key key : table.getKeys())
                addKey(key, newElement.GetMethods());

            prepareConnectors(table.getKeys(), newElement);

            //Далее добавляю таблицу в модель для сериализации.
            EATable eaTable = new EATable();
            eaTable.setIsDeleted(false);
            eaTable.setNotes(dbElement.getNotes());
            eaTable.setName(dbElement.getName());
            eaTable.setId(newElement.GetElementID());
            eaTable.setAttrs(new ArrayList<EAAttribute>());

            //Поля таблицы
            for (Column column : dbElement.getColumns())
                addAttribute(column, newElement.GetAttributes(), eaTable.getAttrs());

            eaElements.add(eaTable);
        }
        //Для вью и хп добавляю SQL-код
        else
        {
            TaggedValue taggedValue = newElement.GetTaggedValues().AddNew(dbElement.getTagType(),"<memo>");
            taggedValue.SetNotes(dbElement.getDefinition());
            taggedValue.Update();

            //Для вью добавляю dependencies
            if (dbElement.getStereoType().equals("view"))
            {
                View view = (View)dbElement;

                TaggedValue tv = newElement.GetTaggedValues().AddNew("parents","<memo>");
                tv.SetNotes(view.getDependencies());
                tv.Update();
            }

            //Далее добавляю вью или процедуру в модель для сериализации.

            EAProcedure eaProcedureOrView = new EAProcedure();
            eaProcedureOrView.setIsDeleted(false);
            eaProcedureOrView.setNotes(dbElement.getNotes());
            eaProcedureOrView.setName(dbElement.getName());
            eaProcedureOrView.setId(newElement.GetElementID());
            eaProcedureOrView.setDefinition(dbElement.getDefinition());
            eaProcedureOrView.setAttrs(new ArrayList<EAAttribute>());

            eaElements.add(eaProcedureOrView);

            //Поля или параметры вью или процедуры.
//            for (Column column : element.getColumns())
//                addAttribute(column, newElement.GetAttributes(), eaProcedureOrView.getAttrs());
//

            //Вытаскиваю комментарий из кода вью или хп.
            try {
                String notes = Parser.getElementComment(dbElement.getDefinition(), dbElement);
                newElement.SetNotes(notes);
                newElement.Update();
                //Обновляю элемент для сериализации
                eaProcedureOrView.setNotes(notes);
            }
            catch (ParsingException e) {
                System.out.println(e.getMessage());
                //e.printStackTrace();
            }
        }
    }

    //Получаю из ключей данные необходимые для создания конекторов в моделе.
    public void prepareConnectors(List<Key> keys, Element element) {
        ArrayList<Key> list;
        DBConnector connector;

        label0:
        for (Key key : keys) {
            if (key.getStereoType().equals("FK")) {
                list = new ArrayList<>(keys.subList(0, keys.size()));
                list.remove(key);

                for (Key anotherKey : list) {
                    //Категоризация
                    if (key.getColumns().hashCode() == anotherKey.getColumns().hashCode() && anotherKey.getStereoType().equals("PK")) {
                        connector = new DBConnector("Aggregation", connectorsCounter++, element.GetElementID(), 2, key.getTargetTable().split("\\.")[0], key.getTargetTable().split("\\.")[1], key.getTargetColumns());
                        connectors.add(connector);
                        continue label0;
                    }
                    //Идентифицирующая связь
                    else if (key.getColumns().hashCode() == anotherKey.getColumns().hashCode() && (anotherKey.getStereoType().equals("UQ") || anotherKey.getStereoType().equals("AK"))) {
                        connector = new DBConnector("Aggregation", connectorsCounter++, element.GetElementID(), 1, key.getTargetTable().split("\\.")[0], key.getTargetTable().split("\\.")[1], key.getTargetColumns());
                        connectors.add(connector);
                        continue label0;
                    }
                }
                //Неидентифицирующая связь
                connector = new DBConnector("Association", connectorsCounter++, element.GetElementID(), 0, key.getTargetTable().split("\\.")[0], key.getTargetTable().split("\\.")[1], key.getTargetColumns());
                connectors.add(connector);
            }
        }
    }

    private void addConnectors(EAModel model) {
        for (DBConnector connector : connectors) {
            for (EAPackage eaPackage : model.getPackages()) {
                for (EAElement eaElement : eaPackage.getElements()) {
                    if (eaPackage.getName().equals(connector.getSupplierSchemaName()) && eaElement.getName().equals(connector.getSupplierTableName())) {
                        Element element = repository.GetElementByID(connector.getClientID());

                        Connector newConnector = element.GetConnectors().AddNew(String.valueOf(connector.getName()), connector.getType());
                        newConnector.SetRouteStyle(connector.getRoutStyle());
                        newConnector.SetClientID(connector.getClientID());
                        newConnector.SetSupplierID(eaElement.getId());
                        newConnector.SetDirection(connector.getDirection());
                        newConnector.GetSupplierEnd().SetNavigable(connector.getNavigable());
                        newConnector.GetSupplierEnd().SetAggregation(connector.getAggregation());
                        newConnector.GetSupplierEnd().SetRole(connector.getSupplierColumns());

                        newConnector.Update();
                        element.GetConnectors().Refresh();
                        element.Update();
                    }
                }
            }
        }
    }

    //Добавление ключа в таблицу
    //Key key - Ключ таблицы.
    //Collection<Method> DBElements - коллекция, в которой EA хранит ключи таблицы.
    private void addKey(Key key, Collection<Method> elements)
    {
        Method newMethod = elements.AddNew(key.getName(), key.getType());

        newMethod.SetStereotype(key.getStereoType());
        newMethod.Update();

        for (Map.Entry<String, String> map : key.getColumns().entrySet()) {
            Parameter param = newMethod.GetParameters().AddNew(map.getKey(), map.getValue());
            param.Update();
        }

        MethodTag mt = newMethod.GetTaggedValues().AddNew(key.getTargetTable(), key.getTargetColumns());
        mt.SetNotes(key.getTargetColumns());

        mt.Update();
        newMethod.GetTaggedValues().Refresh();
    }

    //Lобавление поля таблицы или вью, или параметра процедуры в модель EA.
    //Column column - Добавляемое поле.
    //Collection<Attribute> DBElements - Коллекция, в которой EA хранит поля.
    private void addAttribute(Column column, Collection<Attribute> elements, List<EAAttribute> eaAttributes)
    {
        Attribute newAttribute = elements.AddNew(column.getName(), column.getType());

        //Добавляю алиас если есть. Кастыль для UDT у которого в алиасе сидит значение rule.
        if (!"".equals(column.getAlias()))
            newAttribute.SetStyle(column.getAlias());

        newAttribute.SetStereotype(column.getStereoType());
        newAttribute.SetNotes(column.getNotes());
        newAttribute.SetType(column.getDataType());
        newAttribute.SetLength(column.getPrecision());
        newAttribute.SetLength(column.getLength());
        newAttribute.SetPrecision(column.getPrecision());
        newAttribute.SetIsOrdered(column.isPK());
        newAttribute.SetAllowDuplicates(column.isNotNull());
        newAttribute.SetDefault(column.getDefaultValue());
        newAttribute.Update();
        elements.Refresh();

        //Добавляю аттрибут в модель для сериализации.
        EAAttribute eaAttribute = new EAAttribute(column.getName(), column.getNotes(), column.getDataType(), column.getLength(), column.getPrecision(), column.getDefaultValue(), column.isNotNull(), column.isPK(), newAttribute.GetAttributeID());
        eaAttribute.setIsDeleted(false);
        eaAttributes.add(eaAttribute);
    }

    /*
    Обновление данных элемента моделирования
    Входные параметры:
     1.DBObject element - Элемент полученный из базы.
     2.Element oldElement - Обновляемый элемент.
    */
    private void updateElement(DBObject dbElement, int elementID, EAElement eaElement)
    {
        //Обновляю Notes для элемента моделирования
        Element oldElement = repository.GetElementByID(elementID);

        if (dbElement.getStereoType().equals("table") && (dbElement.getNotes() == null || "".equals(dbElement.getNotes())) && (eaElement.getNotes() != null && !"".equals(eaElement.getNotes())))
            logger.warn("Элемент: " + dbElement.getSchema() + "." + dbElement.getName() + " в модели прокомментирован, а в БД нет!");

        if ((dbElement.getNotes() != null && !"".equals(dbElement.getNotes())) || updateNullComments) {
            oldElement.SetNotes(dbElement.getNotes());
            oldElement.Update();
            //Обновляю элемент для сериализации.
            //eaElement.setNotes(dbElement.getNotes());
        }

        //Если это вью или хп, то обновляю SQL-код.
        if (dbElement.getStereoType().equals("view") || dbElement.getStereoType().equals("procedure"))
        {
            TaggedValue tv;

            for (short i = 0; i < oldElement.GetTaggedValues().GetCount(); i++)
            {
                tv = oldElement.GetTaggedValues().GetAt(i);

                if (tv.GetName().equals("viewdef") || tv.GetName().equals("procdef")) {
                    tv.SetNotes(dbElement.getDefinition());
                    tv.Update();
                }

                //Для вью обновляю dependencies
                if (tv.GetName().equals("parents") && dbElement.getStereoType().equals("view")) {
                    tv.SetValue("<memo>");
                    tv.SetNotes(((View)dbElement).getDependencies());
                    tv.Update();
                }
            }

            oldElement.GetTaggedValues().Refresh();
            oldElement.Update();

            //Обновляю элемент для сериализации.
            //((EAProcedure) eaElement).setDefinition(dbElement.getDefinition());

            //Вытаскиваю комментарий из кода вью или хп.
            try {
                String notes = Parser.getElementComment(dbElement.getDefinition(), dbElement);

                if ((notes != null && !"".equals(notes)) || updateNullComments) {
                    oldElement.SetNotes(notes);
                    oldElement.Update();
                    //Обновляю элемент для сериализации.
                    eaElement.setNotes(notes);
                }
            }
            catch (ParsingException e)
            {
                logger.error("Ошибка при парсинге кода вью или хп.", e);
            }
        } else {
            Table table = (Table)dbElement;

            //Обновляю ключи у таблиц
            for (short i = 0; i < oldElement.GetMethods().GetCount(); i++) {
                if (oldElement.GetMethods().GetAt(i).GetStereotype().equals("FK") || oldElement.GetMethods().GetAt(i).GetStereotype().equals("PK"))
                    oldElement.GetMethods().DeleteAt(i, false);
            }

            for (Key key : table.getKeys())
                addKey(key, oldElement.GetMethods());

            //Обновляю конекторы у элемента
            for (short i = 0; i < oldElement.GetConnectors().GetCount(); i++)
                oldElement.GetConnectors().DeleteAt(i, false);

            prepareConnectors(table.getKeys(), oldElement);
        }
    }

    /*
    Обновляет данные поля(параметра).
    Обновляются: комментарий, тип данных, точность типа.
    Входные параметры:
     1.Column column - Поле полученное из БД.
     2. int attID - ID обновляемого поля.
    */
    private void updateAttribute(Column column, int attID, EAAttribute eaAttribute) {
        Attribute att = repository.GetAttributeByID(attID);

        if ((column.getNotes() == null || "".equals(column.getNotes())) && (eaAttribute.getNotes() != null && !"".equals(eaAttribute.getNotes())))
            logger.warn("Поле: " + repository.GetPackageByID(repository.GetElementByID(att.GetParentID()).GetPackageID()).GetName()
                    + "." + repository.GetElementByID(att.GetParentID()).GetName() + "." + eaAttribute.getName() + " в модели прокомментирован, а в БД нет!");

        if ((column.getNotes() != null && !"".equals(column.getNotes())) || updateNullComments)
            att.SetNotes(column.getNotes());

        //Обновляю алиас если есть. Кастыль для UDT у которого в алиасе сидит значение rule.
        if (!"".equals(column.getAlias()))
            att.SetStyle(column.getAlias());

        att.SetType(column.getDataType());
        att.SetLength(column.getLength());
        att.SetPrecision(column.getPrecision());
        att.SetIsOrdered(column.isPK());
        att.SetAllowDuplicates(column.isNotNull());
        att.SetDefault(column.getDefaultValue());
        att.Update();

        //Обновляю атрибут для сериализации.
        if ((column.getNotes() != null && !"".equals(column.getNotes())) || updateNullComments)
            eaAttribute.setNotes(column.getNotes());

        eaAttribute.setDataType(column.getDataType());
        eaAttribute.setPrecision(column.getPrecision());
    }

    //Добавляет новые типы данных в системную таблицу EA
    private void addUDTinModel(DBObject dbTypes) {
        Datatype dt;

        label0:
        for (Column c : dbTypes.getColumns()) {
            for (short i = 0; i < repository.GetDatatypes().GetCount(); i++) {
                dt = repository.GetDatatypes().GetAt(i);

                if (c.getName().equals(dt.GetName()) && dt.GetProduct().equals("SQL Server 2008")) {
                    /*
                    logger.info("Обновляю тип данных:" + c.getName());
                    dt.SetGenericType(c.getDataType());
                    if (dt.GetSize() != 0) {
                        try {
                            dt.SetDefaultLen(Integer.parseInt(c.getPrecision()));
                            dt.SetDefaultPrec(Integer.parseInt(c.getPrecision()));
                        } catch (NumberFormatException nfe) {

                        }
                    }

                    dt.Update();
                    */
                    continue label0;
                }
            }

            logger.info("Добавляю тип данных:" + c.getName());
            Datatype newDT = repository.GetDatatypes().AddNew(c.getName(), "DDL");
            newDT.SetUserDefined(1);
            newDT.SetType("DDL");
            newDT.SetProduct("SQL Server 2008");
            newDT.SetGenericType(c.getDataType());

            if (!"".equals(c.getPrecision())) {
                try {
                    newDT.SetSize(1);
                    newDT.SetDefaultLen(Integer.parseInt(c.getPrecision()));
                    newDT.SetMaxLen(Integer.parseInt(c.getPrecision()));
                    newDT.SetDefaultPrec(Integer.parseInt(c.getPrecision()));
                } catch (NumberFormatException nfe) {

                }
            }

            newDT.Update();
        }
    }

    //Следующие четыре метода собирают информацию о моделе EA в класс EAModel, для последующего сравнения модели с БД.
    //Располагаю их в этом классе, т.к. хочу собрать в нём все взаимодествия с Enterprise Architect.
    private EAModel getModelHash(Package pack) {
        EAModel model = new EAModel();
        ArrayList<EAPackage> eaPackages = new ArrayList<>();

        for (short i = 0; i < pack.GetPackages().GetCount(); i++) {
            Package p = pack.GetPackages().GetAt(i);
            eaPackages.add(new EAPackage(p.GetName(), getPackageHash(p), p.GetPackageID()));
        }

        model.setPackages(eaPackages);
        model.setHashDate(new Date());
        model.setUdt(getUDT(pack));

        return model;
    }

    private ArrayList<EAElement> getPackageHash(Package pack) {
        ArrayList<EAElement> result = new ArrayList<>();
        Element element;
        TaggedValue tv;
        EAElement eaElement;

        for (short i = 0; i < pack.GetElements().GetCount(); i++) {
            element = pack.GetElements().GetAt(i);

            switch(element.GetStereotype()) {
                case "table":
                    eaElement = new EATable(element.GetName(), element.GetNotes(), getElementHash(element), element.GetElementID());
                    break;
                case "view":
                    eaElement = new EAView(element.GetName(), element.GetNotes(), getElementHash(element), element.GetElementID());

                    for (short j = 0; j < element.GetTaggedValues().GetCount(); j++) {
                        tv = element.GetTaggedValues().GetAt(j);

                        if (tv.GetName().equals("viewdef"))
                            eaElement.setDefinition(tv.GetNotes());

                        if (tv.GetName().equals("parents"))
                            eaElement.setDependencies(tv.GetNotes());
                    }

                    break;
                case "procedure":
                    eaElement = new EAProcedure(element.GetName(), element.GetNotes(), getElementHash(element), element.GetElementID());

                    for (short j = 0; j < element.GetTaggedValues().GetCount(); j++) {
                        tv = element.GetTaggedValues().GetAt(j);

                        if (tv.GetName().equals("procdef"))
                            eaElement.setDefinition(tv.GetNotes());
                    }

                    break;
                default:
                    continue;
            }

            result.add(eaElement);
        }

        return result;
    }

    private ArrayList<EAAttribute> getElementHash(Element element) {
        ArrayList<EAAttribute> result = new ArrayList<>();

        for (short i = 0; i < element.GetAttributes().GetCount(); i++) {
            Attribute att = element.GetAttributes().GetAt(i);
            result.add(new EAAttribute(att.GetName(), att.GetNotes(), att.GetType(), att.GetLength(), att.GetPrecision(), att.GetDefault(), att.GetAllowDuplicates(), att.GetIsOrdered(), att.GetAttributeID()));
        }

        return result;
    }

    private EATable getUDT(Package pack) {
        EATable udt = new EATable();
        Element element;

        for (short i = 0; i < pack.GetElements().GetCount(); i++) {
            element = pack.GetElements().GetAt(i);

            if (element.GetName().equals("UDT")) {
                udt.setName("UDT");
                udt.setAttrs(getElementHash(element));
                udt.setId(element.GetElementID());
            }
        }

        return udt;
    }

    public void setUpdateNullComments(boolean updateNullComments) {
        this.updateNullComments = updateNullComments;
    }
}
