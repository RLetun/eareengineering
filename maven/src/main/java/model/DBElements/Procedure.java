package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 21.11.2016.
 * Класс для хранения данных о процедуре БД
 */
public class Procedure extends DBObject
{
    private String definition;

    private final String tagType = "procdef";

    public Procedure(String name, String definition, String schema) {
        super(name, "", "procedure", schema);
        this.definition = definition;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

//        for (Column c : this.getColumns())
//            hashCode += c.hashCode();

        hashCode = 31 * hashCode + (definition != null ? definition.hashCode() : 0);

        return hashCode;
    }

    @Override
    public String getDefinition() {
        return definition;
    }

    @Override
    public String getTagType() {
        return this.tagType;
    }
}
