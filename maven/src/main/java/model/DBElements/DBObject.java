package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 16.11.2016.
 * Базовый класс для классов Table, View, Procedure
 */
public abstract class DBObject
{
    private String name; //Наименование объекта.
    private String notes; //Комментарий объекта, получается из extendedProperty. У вью и хп комментарий хранится вместе с кодом в поле definition.
    private String stereoType; //Стереотип
    private String schema;

    private List<Column> columns;

    private final String type = "Class"; //Тип объкта в EA.

    DBObject(String name, String notes, String stereoType, String schema) {
        this.name = name;
        this.notes = notes;
        this.stereoType = stereoType;
        this.schema = schema;
    }

    //сетеры

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    //Гетеры

    //Возвращает тип TaggedValue. Переопределён в классах View и Procedure.
    public abstract String getTagType();

    //Возвращает SQL-запрос с помощью которого создавались view или ХП. Переопределён в классах View и Procedure.
    public abstract String getDefinition();

    public List<Column> getColumns() {
        return columns;
    }

    public String getType() {
        return type;
    }

    public String getSchema() {
        return schema;
    }

    public String getStereoType() {
        return stereoType;
    }

    public String getName() {
        return name;
    }

    public String getNotes() {
        return notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DBObject dbObject = (DBObject) o;

        return columns != null ? columns.equals(dbObject.columns) : dbObject.columns == null;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        if (columns == null)
            return hashCode;

        for (Column c : columns)
            hashCode += c.hashCode();

        hashCode = 31 * hashCode + (notes != null ? notes.hashCode() : 0);

        return hashCode;
        //return columns != null ? columns.hashCode() : 0;
    }
}
