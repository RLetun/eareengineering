package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 29.12.2016.
 * Содержит DBObject (таблицы, вью и процедуры) принадлежащие одной схеме данных
 */
public class Schema {
    private String name;
    private List<DBObject> objects;

    public Schema(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<DBObject> getObjects() {
        return objects;
    }

    public void setObjects(List<DBObject> objects) {
        this.objects = objects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Schema schema = (Schema) o;

        return objects != null ? objects.equals(schema.objects) : schema.objects == null;

    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        if (objects == null)
            return hashCode;

        for (DBObject o : objects)
            hashCode += o.hashCode();

        return hashCode;

        //return objects != null ? objects.hashCode() : 0;
    }
}
