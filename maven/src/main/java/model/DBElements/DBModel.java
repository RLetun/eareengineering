package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 29.12.2016.
 * Контейнер для схем данных. Используется для сравнения модели EA с базой.
 * Так же хранит данные о пользовательских типах (UDT).
 */
public class DBModel {
    private List<Schema> schemas;
    private Table udt;

    public DBModel(List<Schema> schemas) {
        this.schemas = schemas;
    }

    public DBModel(List<Schema> schemas, Table udt) {
        this.schemas = schemas;
        this.udt = udt;
    }

    public List<Schema> getSchemas() {
        return schemas;
    }

    public Table getUdt() {
        return udt;
    }
}
