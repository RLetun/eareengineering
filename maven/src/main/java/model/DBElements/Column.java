package model.DBElements;

/**
 * Created by romanletun on 16.11.2016.
 * Данные поля таблицы или view или параметра ХП.
 */
public class Column
{
    private String name;
    private String stereoType;
    private String dataType;
    private String length;
    private String precision;
    private String notes;
    private String defaultValue;
    private String alias = "";
    private boolean isNotNull;
    private boolean isPK;
    private int hash;

    private final String type = "Attribute";

    public Column(String name, String notes, String dataType, String length, String precision, String defaultValue, int isNullable, int isPK) {
        this.name = name;
        this.notes = notes;
        this.stereoType = "column";
        this.dataType = dataType;
        this.length = length;
        this.precision = precision;
        this.isNotNull = (isNullable == 1 ? true : false);
        this.defaultValue = defaultValue;
        this.isPK = (isPK == 1 ? true : false);
        this.hash = hashCode();
    }

    public Column(String name, String notes, String alias, String dataType, String length, String precision, String defaultValue, int isNullable, int isPK) {
        this.name = name;
        this.notes = notes;
        this.alias = alias;
        this.stereoType = "column";
        this.dataType = dataType;
        this.length = length;
        this.precision = precision;
        this.isNotNull = (isNullable == 1 ? true : false);
        this.defaultValue = defaultValue;
        this.isPK = (isPK == 1 ? true : false);
        this.hash = hashCode();
    }

    //Для параметров процедур.
    public Column(String name, String dataType, String precision) {
        this.name = name;
        this.stereoType = "column";
        this.dataType = dataType;
        this.precision = precision;
        this.hash = hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Column column = (Column) o;

        if (isNotNull != column.isNotNull) return false;
        if (isPK != column.isPK) return false;
        if (name != null ? !name.equals(column.name) : column.name != null) return false;
        if (dataType != null ? !dataType.equals(column.dataType) : column.dataType != null) return false;
        if (precision != null ? !precision.equals(column.precision) : column.precision != null) return false;
        if (notes != null ? !notes.equals(column.notes) : column.notes != null) return false;
        if (defaultValue != null ? !defaultValue.equals(column.defaultValue) : column.defaultValue != null)
            return false;
        return type != null ? type.equals(column.type) : column.type == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        result = 31 * result + (precision != null ? precision.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (isNotNull ? 1 : 0);
        result = 31 * result + (isPK ? 1 : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public boolean isPK() {
        return isPK;
    }

    public String getNotes() {
        return notes;
    }

    public String getStereoType() {
        return stereoType;
    }

    public boolean isNotNull() {
        return isNotNull;
    }

    public int getHash() {
        return hash;
    }

    public String getType() {
        return type;
    }

    public String getDataType() {
        return dataType;
    }

    public String getPrecision() {
        return precision;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getLength() {
        return length;
    }

    public String getAlias() {
        return alias;
    }
}
