package model.DBElements;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by romanletun on 17.11.2016.
 * Даные о ключе таблице.
 */
public class Key {

    private String name;
    private String stereoType;
    private String targetTable;
    private String targetColumns;
    private Map<String, String> columns = new HashMap<>();

    private final String type = "Method";

    public Key(String name, String stereoType) {
        this.name = name;
        this.stereoType = stereoType;
    }

    public void addColumn(String column, String dataType)
    {
        columns.put(column, dataType);
    }

    public Map<String, String> getColumns() {
        return columns;
    }

    public String getName() {
        return name;
    }

    public String getStereoType() {
        return stereoType;
    }

    public String getType() {
        return type;
    }

    public String getTargetTable() {
        return targetTable;
    }

    public void setTargetTable(String targetTable) {
        this.targetTable = targetTable;
    }

    public String getTargetColumns() {
        return targetColumns;
    }

    public void setTargetColumns(String targetColumns) {
        this.targetColumns = targetColumns;
    }
}
