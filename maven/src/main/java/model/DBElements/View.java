package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 16.11.2016.
 * Класс для хранения данных о view
 */
public class View extends DBObject
{
    private String dependencies;
    private String definition;

    private final String tagType = "viewdef";

    public View(String name, String dependencies, String definition, String schema) {
        super(name, "", "view", schema);
        this.definition = definition;
        this.dependencies = dependencies;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

//        for (Column c : this.getColumns())
//            hashCode += c.hashCode();

        hashCode = 31 * hashCode + (definition != null ? definition.hashCode() : 0);
        hashCode = 31 * hashCode + (dependencies != null ? dependencies.hashCode() : 0);

        return hashCode;
    }

    @Override
    public String getDefinition() {
        return definition;
    }

    @Override
    public String getTagType() {
        return tagType;
    }

    public String getDependencies() {
        return dependencies;
    }
}
