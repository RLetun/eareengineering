package model.DBElements;

import java.util.List;

/**
 * Created by romanletun on 16.11.2016.
 * Класс для хранения данных о таблицах БД.
 */
public class Table extends DBObject
{
    private List<Key> keys;

    public Table(String name, String notes, String schema) {
        super(name, notes, "table", schema);
    }

    public List<Key> getKeys() {
        return keys;
    }

    public void setKeys(List<Key> keys) {
        this.keys = keys;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getTagType() {
        return null;
    }
}
