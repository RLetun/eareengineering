package model.DBElements;

/**
 * Created by romanletun on 03.05.2017.
 */
public class DBConnector {
    private String type;
    private final int routStyle = 3;
    private int clientID;
    private final String direction = "Source -> Destination";
    private int name;
    private String supplierTableName;
    private String supplierSchemaName;
    private String supplierColumns;
    private int supplierID;
    private int aggregation;
    private final String navigable = "Navigable";

    public DBConnector(String type, int name, int clientID, int aggregation, String supplierSchemaName, String supplierTableName, String supplierColumns) {
        this.type = type;
        this.name = name;
        this.clientID = clientID;
        this.aggregation = aggregation;
        this.supplierSchemaName = supplierSchemaName;
        this.supplierTableName = supplierTableName;
        this.supplierColumns = supplierColumns;
    }

    public String getSupplierTableName() {
        return supplierTableName;
    }

    public void setSupplierTableName(String supplierTableName) {
        this.supplierTableName = supplierTableName;
    }

    public String getSupplierSchemaName() {
        return supplierSchemaName;
    }

    public void setSupplierSchemaName(String supplierSchemaName) {
        this.supplierSchemaName = supplierSchemaName;
    }

    public String getSupplierColumns() {
        return supplierColumns;
    }

    public void setSupplierColumns(String supplierColumns) {
        this.supplierColumns = supplierColumns;
    }

    public int getAggregation() {
        return aggregation;
    }

    public String getNavigable() {
        return navigable;
    }

    public String getType() {
        return type;
    }

    public int getRoutStyle() {
        return routStyle;
    }

    public String getDirection() {
        return direction;
    }

    public int getName() {
        return name;
    }

    public int getClientID() {
        return clientID;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }
}
