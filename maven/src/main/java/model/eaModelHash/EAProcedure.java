package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.ArrayList;

/**
 * Created by romanletun on 16.01.2017.
 */
@JsonAutoDetect
public class EAProcedure extends EAElement {
    private String definition;

    public EAProcedure(String name, String notes, ArrayList<EAAttribute> attrs, int id) {
        super(name, notes, attrs, id);
    }

    public EAProcedure() {
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
//
//        for (EAAttribute a : this.getAttrs())
//            hashCode += a.hashCode();

        hashCode = 31 * hashCode + (definition != null ? definition.hashCode() : 0);

        return hashCode;

        //return attrs != null ? attrs.hashCode() : 0;
    }

    @Override
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public String getDefinition() {
        return definition;
    }

    @Override
    public void setDependencies(String Dependencies) {

    }

    @Override
    public String getDependencies() {
        return null;
    }
}
