package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;

/**
 * Created by romanletun on 28.12.2016.
 */
@JsonAutoDetect
public abstract class EAElement {
    private String name;
    @JsonDeserialize(as = ArrayList.class)
    private ArrayList<EAAttribute> attrs;
    private int id;
    private String notes;
    @JsonIgnore
    private boolean isDeleted = true;

    public EAElement(String name, String notes, ArrayList<EAAttribute> attrs, int id) {
        this.name = name;
        this.notes = notes;
        this.attrs = attrs;
        this.id = id;
    }

    public EAElement() {
    }

    public abstract void setDefinition(String definition);

    public abstract String getDefinition();

    public abstract void setDependencies(String dependencies);

    public abstract String getDependencies();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EAElement eaElement = (EAElement) o;

        return attrs != null ? attrs.equals(eaElement.attrs) : eaElement.attrs == null;

    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        if (attrs == null)
            return hashCode;

        for (EAAttribute a : attrs)
            hashCode += a.hashCode();

        hashCode = 31 * hashCode + (notes != null ? notes.hashCode() : 0);

        return hashCode;

        //return attrs != null ? attrs.hashCode() : 0;
    }

    public void setDeleteFlagFalse()
    {
        for (EAAttribute attribute : attrs)
        {
            attribute.setIsDeleted(false);
        }
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<EAAttribute> getAttrs() {
        return attrs;
    }

    public void setAttrs(ArrayList<EAAttribute> attrs) {
        this.attrs = attrs;
    }

    public int getId() {
        return id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public void setId(int id) {
        this.id = id;
    }
}
