package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by romanletun on 28.12.2016.
 */
@JsonAutoDetect
public class EAAttribute {
    private String name;
    private String dataType;
    private String precision;
    private String length;
    private String notes;
    private String initial;
    private boolean isNotNull;
    private boolean isPK;
    private int id;
    private int hash;
    @JsonIgnore
    private boolean isDeleted = true;

    public EAAttribute(String name, String notes, String dataType, String length, String precision, String initial, boolean isNotNull, boolean isPK, int id) {
        this.name = name;
        this.notes = notes;
        this.dataType = dataType;
        this.length = length;
        this.precision = precision;
        this.initial = initial;
        this.id =  id;
        this.isNotNull = isNotNull;
        this.isPK = isPK;
        this.hash = hashCode();
    }

    public EAAttribute() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EAAttribute that = (EAAttribute) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (dataType != null ? !dataType.equals(that.dataType) : that.dataType != null) return false;
        return precision != null ? precision.equals(that.precision) : that.precision == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        result = 31 * result + (precision != null ? precision.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (initial != null ? initial.hashCode() : 0);
        result = 31 * result + (isNotNull ? 1 : 0);
        result = 31 * result + (isPK ? 1 : 0);
        return result;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public boolean isNotNull() {
        return isNotNull;
    }

    public void setNotNull(boolean notNull) {
        isNotNull = notNull;
    }

    public boolean isPK() {
        return isPK;
    }

    public void setPK(boolean PK) {
        isPK = PK;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }
}
