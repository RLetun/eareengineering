package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.ArrayList;

/**
 * Created by romanletun on 16.01.2017.
 */
@JsonAutoDetect
public class EATable extends EAElement {

    public EATable(String name, String notes, ArrayList<EAAttribute> attrs, int id) {
        super(name, notes, attrs, id);
    }

    public EATable() {
    }

    @Override
    public void setDefinition(String definition) {

    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public void setDependencies(String dependencies) {

    }

    @Override
    public String getDependencies() {
        return null;
    }
}
