package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by romanletun on 28.12.2016.
 * Контейнер для пакетов. Используется для сравнения модели EA с базой.
 * Так же хранит данные о пользовательских типах (UDT).
 */
@JsonAutoDetect
public class EAModel {
    private EATable udt;
    private Date hashDate;
    @JsonDeserialize(as = ArrayList.class)
    private List<EAPackage> packages;

    public EAModel() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EAModel eaModel = (EAModel) o;

        return packages != null ? packages.equals(eaModel.packages) : eaModel.packages == null;

    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        if (packages == null)
            return hashCode;

        for (EAPackage p : packages)
            hashCode += p.hashCode();

        return hashCode;
        //return packages != null ? packages.hashCode() : 0;
    }

    public Date getHashDate() {
        return hashDate;
    }

    public void setHashDate(Date hashDate) {
        this.hashDate = hashDate;
    }

    public List<EAPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<EAPackage> packages) {
        this.packages = packages;
    }

    public EATable getUdt() {
        return udt;
    }

    public void setUdt(EATable udt) {
        this.udt = udt;
    }
}
