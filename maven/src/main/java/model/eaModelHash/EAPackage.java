package model.eaModelHash;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romanletun on 28.12.2016.
 */
@JsonAutoDetect
public class EAPackage {
    private String name;
    @JsonDeserialize(as = ArrayList.class)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property="type")
    @JsonSubTypes({
            @JsonSubTypes.Type(value=EATable.class, name="table"),
            @JsonSubTypes.Type(value=EAProcedure.class, name="procOrView")
    })
    private List<EAElement> elements;
    private int id;
    private int hash;

    public EAPackage(String name, List<EAElement> elements, int id) {
        this.name = name;
        this.elements = elements;
        this.id = id;
        this.hash = hashCode();
    }

    public EAPackage() {
    }

    public void setDeleteFlagFalse()
    {
        for (EAElement eaElement : elements)
        {
            eaElement.setIsDeleted(false);

            for (EAAttribute attribute : eaElement.getAttrs())
            {
                attribute.setIsDeleted(false);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EAPackage eaPackage = (EAPackage) o;

        return elements != null ? elements.equals(eaPackage.elements) : eaPackage.elements == null;

    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        if (elements == null)
            return hashCode;

        for (EAElement e : elements)
            hashCode += e.hashCode();

        return hashCode;

        //return elements != null ? elements.hashCode() : 0;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EAElement> getElements() {
        return elements;
    }

    public void setElements(List<EAElement> elements) {
        this.elements = elements;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
