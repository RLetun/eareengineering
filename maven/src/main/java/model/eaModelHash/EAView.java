package model.eaModelHash;

import java.util.ArrayList;

/**
 * Created by romanletun on 03.04.2017.
 */
public class EAView extends EAElement {
    private String definition;
    private String dependencies;

    public EAView(String name, String notes, ArrayList<EAAttribute> attrs, int id) {
        super(name, notes, attrs, id);
    }

    public EAView() {
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
//
//        for (EAAttribute a : this.getAttrs())
//            hashCode += a.hashCode();

        hashCode = 31 * hashCode + (definition != null ? definition.hashCode() : 0);
        hashCode = 31 * hashCode + (dependencies != null ? dependencies.hashCode() : 0);

        return hashCode;

        //return attrs != null ? attrs.hashCode() : 0;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getDependencies() {
        return dependencies;
    }

    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }
}
