package model;

import model.DBElements.DBObject;
import exceptions.ParsingException;

/**
 * Created by romanletun on 15.11.2016.
 * Вытаскиват из кода объявления вью или хп комментарий к вью или хп.
 */
abstract class Parser {
    //Кидает ParsingException
    //На входе:
    //1.String elementDef - текст SQL-запроса создания вью или ХП.
    //2.String elementName - Наименование вью или ХП. Необходимо для большей информативности сообщений об ошибках.
    static String getElementComment(String elementDef, DBObject element) throws ParsingException
    {
        //Копирую значение elementDef для обработки, что бы сохранить исходный текст запроса, т.к. в дальнейшем буду ещё вытаскивать из неё комментарии для полей и параметров.
        String result = elementDef;

        if (!result.substring(0,10).matches("(?iu)select")) {
            //Если комментарий оформлен вот так:
            /*
            -- =============================================
            -- Author:          <Author,,Name>
            -- Create date: <Create Date,,>
            -- Description:     <Description,,>
            -- =============================================
            */
            //то выбираю весь текст между многократными повторениями символа "=".
            if (result.split("={3,}").length > 1) {
                result = result.split("={3,}")[result.split("={3,}").length - 2];

                //Если в тексте комментария присутствует слово "Назначение" или "Description", то комментарий будет после него,
                //Но не включая "Изменения".
                if (result.split("(?iu)(назначение\\:*)|(description\\:*)").length > 1) {
                    result = result.split("(?iu)(назначение\\:*)|(description\\:*)")[1];
                    result = result.split("(?iu)изменения\\:*")[0];
                }

                //Убираю пустые строки и лишние пробельные символы.
                result = result.replaceAll("\r\n\\s*\\-{2,}", "\r\n");
                result = result.replaceAll("^\\s*\\-{2,}", "");
                result = result.replaceAll("\r\n\\s*\r\n", "");
                result = result.replaceAll("\r\n\\s*", "");
                result = result.replaceAll("^\r\n", "");
                result = result.replaceAll("^\\s*", "");
            }
            //Если комментарий оформлен не так как показано выше (В тексте нет многократно повторяющегося символа "="),
            //то выбираю первый многострочный комментарий ДО первого select.
            else if (result.split("(?iu)select")[0].split("\\*\\/")[0].split("\\/\\*").length > 1) {
                //Всё что между /* */ перед первым select
                result = result.split("(?iu)select")[0].split("\\*\\/")[0].split("\\/\\*")[1]; //.replace("\r\n","^^^");//.match(/\/\*.*/);
                result = result.replaceAll("(\\*\\s{1,})|[\\*\\/]", "").replace("\\s{2,}", " ");
                //definition = definition.replace(/\^\^\^/g,"\r\n");

                //Если в тексте комментария присутствует слово "Назначение" или "Description", то комментарий будет после него,
                //Но не включая "Изменения".
                if (result.split("(?iu)(назначение\\:*)|(Description\\:*)").length > 1) {
                    result = result.split("(?iu)(назначение\\:*)|(Description\\:*)")[1];
                    result = result.split("(?iu)изменения\\:*")[0];
                }

                //Убираю пустые строки и лишние пробельные символы.
                result = result.replaceAll("\r\n\\s*\\-{2,}", "\r\n");
                result = result.replaceAll("^\\s*\\-{2,}", "");
                result = result.replaceAll("\r\n\\s*\r\n", "");
                result = result.replaceAll("^\r\n", "");
                result = result.replaceAll("^\\s", "");
            }
            else
                throw new ParsingException("В поле definition элемента " + element.getSchema() + "." + element.getName() + " отсуствует комментарий");
        }
        else
            throw new ParsingException("В поле definition элемента " + element.getSchema() + "." + element.getName() + " присутствует селект запрос вместо объявления вью или хп.");

        return result;
    }
}
