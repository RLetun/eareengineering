package model;

import model.DBElements.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by romanletun on 16.11.2016.
 * В этом классе собраны все взаимодействия с БД.
 */
class DAO
{
    private final String connectionString = "jdbc:sqlserver://ZSQL;databaseName=CMIOR_A;";
    private final String connectionStringEnArchitect = "jdbc:sqlserver://ZSQL;databaseName=EnArchitect;";
    private final String userName = "sa";
    private final String password = "Qwerty11";
    private Connection connection;
    //Логгер
    private static Logger logger = LoggerFactory.getLogger(DAO.class);

    //Запрос на получение наименвоаний всех схемм данных, у которых owner = dbo.
    private  final String schemaQuery = "select SCHEMA_NAME from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_OWNER = 'dbo'";
    //Запрос на получение для таблиц и вью принадлежащих (?) схеме данных: имени, комментария (из exndedProperty), типа (table или view) и SQL-када запроса (только для view).
    private final String tableAndViewQuery = "select TABLE_NAME, cast(n.value as varchar(max)), case when t.TABLE_TYPE = 'BASE TABLE' then 'table' when t.TABLE_TYPE = 'VIEW' then 'view' end as steretype, (select VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = t.TABLE_NAME and v.TABLE_SCHEMA = t.TABLE_SCHEMA) as definition, (select isnull(referenced_schema_name + '.', '') + referenced_entity_name + ';' from sys.sql_expression_dependencies where referencing_id = OBJECT_ID(t.TABLE_SCHEMA + '.' + t.table_name) for xml path('')) as dependencies from Information_Schema.Tables t left join fn_listextendedproperty (NULL, 'schema', ?, 'table', null, null, null) n on t.TABLE_NAME = n.objname collate database_default where TABLE_SCHEMA = ?";
    //Поля таблиц и вью: Имя, комментарий (из exndedProperty), тип данных, точность типа, isNull
    private final String columnQuery = "select c.COLUMN_NAME, cast(n.value as varchar(max)), isnull(c.DOMAIN_NAME, c.DATA_TYPE) as DataType, isnull(c.CHARACTER_MAXIMUM_LENGTH, '') as length, cast(isnull(c.NUMERIC_PRECISION, '') as varchar(100)) as precision, isnull(c.COLUMN_DEFAULT, '') as defaultValue, case when c.IS_NULLABLE = 'YES' then 0 else 1 end as allowsNull, case when c.COLUMN_NAME in (select COLUMN_NAME from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cc where cc.TABLE_SCHEMA = c.TABLE_SCHEMA and cc.TABLE_NAME = c.TABLE_NAME and LEFT(CONSTRAINT_NAME, 2) = 'PK') then 1 else 0 end as isPK from INFORMATION_SCHEMA.COLUMNS c left join fn_listextendedproperty (NULL, 'schema', ?, ?, ?, 'column', null) n on c.COLUMN_NAME = n.objname collate database_default where c.TABLE_SCHEMA = ? and c.TABLE_NAME = ?";
    //Список ключей таблицы, первичный и внешние
    private final String keysQuery = "select CONSTRAINT_NAME, LEFT(CONSTRAINT_NAME, 2) as stereotype from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_SCHEMA = ? and TABLE_NAME = ?";
    //Поля входящие в состав ключа
    private final String keysColumnsQuery = "select cc.COLUMN_NAME ,c.DATA_TYPE from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cc inner join INFORMATION_SCHEMA.COLUMNS c on cc.COLUMN_NAME = c.COLUMN_NAME and cc.TABLE_NAME = c.TABLE_NAME and cc.TABLE_SCHEMA = c.TABLE_SCHEMA and cc.TABLE_CATALOG = c.TABLE_CATALOG where cc.TABLE_SCHEMA = ? and cc.TABLE_NAME = ? and cc.CONSTRAINT_NAME = ?";
    //Праметры процедуры
    private final String procParamsQuery = "select PARAMETER_NAME ,DATA_TYPE ,cast(coalesce(p.CHARACTER_MAXIMUM_LENGTH, p.NUMERIC_PRECISION, p.DATETIME_PRECISION, '') as varchar(100)) as precision from INFORMATION_SCHEMA.PARAMETERS p where SPECIFIC_SCHEMA = ? and SPECIFIC_NAME = ?";
    //Процедуры принадлежащие схеме данных.
    private final String procedureQuery = "select p.name ,m.definition from sys.procedures p inner join sys.schemas s on p.schema_id = s.schema_id inner join sys.sql_modules m on p.object_id = m.object_id where s.name = ?";
    //Возвращает максимальную дату изменения елемента или аттрибута находящихся в пакете EA.
    private final String maxModifiedDateQuery = "with Class as (select [Object_ID] ,Package_ID ,PDATA1 ,o.ModifiedDate from t_object o where o.PDATA1 = ? union all select o.[Object_ID] ,o.Package_ID ,o.PDATA1 ,o.ModifiedDate from t_object o inner join Class a on o.Package_ID = cast(a.PDATA1 as varchar(10)) where o.Object_Type = 'Class' or o.Object_Type = 'Package') select MAX(ModifiedDate) from (select ModifiedDate as ModifiedDate from Class union select ModifiedDate as ModifiedDate from dbo.t_attribute att inner join Class a on att.[Object_ID] = a.[Object_ID]) as A";
    //Возвращает данные о User Defined Types
    private final String userDefinedTypesQuery = "with rules as ( select  o.name  ,som.definition from   sys.objects  o join sys.sql_modules  som   on (o.object_id= som.object_id) where   type = 'R'  )  select  d.DOMAIN_NAME as name  ,d.DATA_TYPE as type, isnull(d.CHARACTER_MAXIMUM_LENGTH, '') as length, cast(isnull(d.NUMERIC_PRECISION, '') as varchar(100)) as precision, d.DOMAIN_DEFAULT as [default]  ,r.definition as [rule] from  INFORMATION_SCHEMA.DOMAINS d left join INFORMATION_SCHEMA.DOMAIN_CONSTRAINTS dc  on d.DOMAIN_NAME = dc.DOMAIN_NAME  left join rules r  on dc.CONSTRAINT_NAME = r.name where  d.DATA_TYPE != 'table type'";
    //Возвращает имя таблицы имена полей на которые ссылается foreign key
    private final String foreignKeyTargetQuery = "select distinct Table_Schema + '.' + TABLE_NAME as tableName ,(select COLUMN_NAME + ',' from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE uu where uu.CONSTRAINT_NAME = u.CONSTRAINT_NAME and uu.TABLE_SCHEMA = u.TABLE_SCHEMA and uu.TABLE_CATALOG = u.TABLE_CATALOG and uu.TABLE_NAME = uu.TABLE_NAME for XMl path('')) from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE u inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS r on u.CONSTRAINT_NAME = r.UNIQUE_CONSTRAINT_NAME and u.TABLE_SCHEMA = r.UNIQUE_CONSTRAINT_SCHEMA and u.TABLE_CATALOG = r.CONSTRAINT_CATALOG where r.CONSTRAINT_NAME = ?";

    DAO() throws SQLException
    {
        connect();
    }

    //Подключение к базе данных
    private void connect() throws SQLException
    {
        try
        {
            //Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
            connection = DriverManager.getConnection(connectionString, userName, password);
        }
        catch (Exception e)
        {
            logger.error("Ошибка при попытке установить соединение с базой данных: ", e);
            throw e;
        }
    }

    void close()
    {
        try
        {
            connection.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    //Возвращает последнюю дату изменения элемента или атрибута принадлежащего пакету packageID. Рекурсивно.
    Date getLastModifiedDate(int packageID)
    {
        Date date = null;

        //Отдельный коннекшн  т.к. запрос к другой базе.
        try ( Connection conn = DriverManager.getConnection(connectionStringEnArchitect, userName, password);)
        {
            PreparedStatement preparedStatement = conn.prepareStatement(maxModifiedDateQuery);
            preparedStatement.setString(1, String.valueOf(packageID));

            ResultSet set = preparedStatement.executeQuery();

            while (set.next())
                date = set.getDate(1);

            preparedStatement.close();
            set.close();
            conn.close();

        } catch (SQLException e)
        {
            logger.error("Ошибка SQL, при получении последней даты изменения элементов или атрибутов в модели EA: ", e);
        }

        return date;
    }

    Table getUDT() {
        Table table = new Table("UDT", "", "");

        List<Column> columns = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(userDefinedTypesQuery);
            ResultSet set = preparedStatement.executeQuery();

            while (set.next())
                columns.add(new Column(set.getString(1), "", set.getString(6), set.getString(2), set.getString(3), set.getString(4), set.getString(5), 0, 0));

            preparedStatement.close();
            set.close();

            table.setColumns(columns);

        } catch (SQLException e)
        {
            logger.error("Ошибка SQL, при получении UDT: ", e);
            return null;
        }

        return table;
    }

    //Реинжиниринг схем данных
    //Добавляет объекты БД схемам в переданном списке.
    void reengineering(List<Schema> schemas)
    {
        for (Schema schema : schemas)
            schema.setObjects(getElementsFromSchema(schema.getName()));
    }

    //Возвращает список схем данных из БД
    List<Schema> getSchemas()
    {
        List<Schema> result = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(schemaQuery);
            ResultSet set = preparedStatement.executeQuery();

            while (set.next())
                result.add(new Schema(set.getString(1)));

            preparedStatement.close();
            set.close();

        } catch (SQLException e)
        {
            logger.error("Ошибка SQL, при получении наименований схем данных: ", e);
        }

        return result;
    }

    //Возвращает список полей (или параметров) принадлежащих: таблице или вью, или хп.
    //На входе: Обект БД чьи поля или параметры надо стать и наименование схемы данных.
    private List<Column> getColumnsFromElement(DBObject element, String schema)
    {
        List<Column> result = new ArrayList<>();

        try {
            PreparedStatement preparedStatement;
            ResultSet set;

            if (element.getStereoType().equals("procedure"))
            {
                preparedStatement = connection.prepareStatement(procParamsQuery);
                preparedStatement.setString(1, schema);
                preparedStatement.setString(2, element.getName());
                set = preparedStatement.executeQuery();

                while (set.next())
                    result.add(new Column(set.getString(1), set.getString(2), set.getString(3)));
            }
            else {
                preparedStatement = connection.prepareStatement(columnQuery);
                preparedStatement.setString(1, schema);
                preparedStatement.setString(2, element.getStereoType());
                preparedStatement.setString(3, element.getName());
                preparedStatement.setString(4, schema);
                preparedStatement.setString(5, element.getName());
                set = preparedStatement.executeQuery();

                while (set.next())
                    result.add(new Column(set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getString(5), set.getString(6), set.getInt(7), set.getInt(8)));
            }

            preparedStatement.close();
            set.close();
        }
        catch (SQLException e)
        {
            logger.error("Ошибка SQL, при реинжиниринге полей таблиц и вью: ", e);
        }

        return result;
    }

    //Возвращает список ключей таблицы (первичный и внешние)
    //На входе таблица и наименование схемы.
    private List<Key> getKeysFromTable(Table table, String schema)
    {
        List<Key> result = new ArrayList<>();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(keysQuery);
            preparedStatement.setString(1, schema);
            preparedStatement.setString(2, table.getName());
            ResultSet set = preparedStatement.executeQuery();

            while (set.next())
            {
                Key key = new Key(set.getString(1), set.getString(2));

                PreparedStatement columnsPS = connection.prepareStatement(keysColumnsQuery);
                columnsPS.setString(1, schema);
                columnsPS.setString(2, table.getName());
                columnsPS.setString(3, key.getName());
                ResultSet columnsSet = columnsPS.executeQuery();

                while (columnsSet.next())
                    key.addColumn(columnsSet.getString(1), columnsSet.getString(2));

                result.add(key);

                columnsPS.close();
                columnsSet.close();

                if (key.getStereoType().equals("FK")) {
                    PreparedStatement ps = connection.prepareStatement(foreignKeyTargetQuery);
                    ps.setString(1, key.getName());
                    ResultSet rs = ps.executeQuery();

                    while (rs.next()) {
                        key.setTargetTable(rs.getString(1));
                        key.setTargetColumns(rs.getString(2).substring(0, rs.getString(2).length() - 1));
                    }
                }
            }

            preparedStatement.close();
            set.close();

        } catch (SQLException e)
        {
            logger.error("Ошибка SQL, при реинжиниринге ключей таблицы: ", e);
        }

        return result;
    }

    //Возвращает все таблицы, view и хранимые процедуры принадлежащие схеме данных
    //На входе наименование схемы данных.
    List<DBObject> getElementsFromSchema(String schema)
    {
        List<DBObject> result = new ArrayList<>();

        try {
            //Вью и таблицы
            PreparedStatement preparedStatement = connection.prepareStatement(tableAndViewQuery);
            preparedStatement.setString(1, schema);
            preparedStatement.setString(2, schema);
            ResultSet set = preparedStatement.executeQuery();

            while (set.next()) {
                if (set.getString(3).equals("table")) {
                    Table table = new Table(set.getString(1), set.getString(2), schema);
                    table.setColumns(getColumnsFromElement(table, schema));
                    table.setKeys(getKeysFromTable(table, schema));
                    result.add(table);
                }
                else {
                    View view = new View(set.getString(1), set.getString(5), set.getString(4), schema);
                    view.setColumns(getColumnsFromElement(view, schema));
                    result.add(view);
                }
            }

            //Процедуры
            preparedStatement = connection.prepareStatement(procedureQuery);
            preparedStatement.setString(1, schema);
            set = preparedStatement.executeQuery();

            while (set.next()) {
                Procedure procedure = new Procedure(set.getString(1), set.getString(2), schema);
                procedure.setColumns(getColumnsFromElement(procedure, schema));
                result.add(procedure);
            }

            preparedStatement.close();
            set.close();
        }
        catch (SQLException e)
        {
            logger.error("Ошибка SQL, при реинжиниринге таблиц, вью и процедур: " , e);
        }

        return result;
    }
}
